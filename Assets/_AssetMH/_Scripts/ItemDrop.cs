using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class ItemDrop : MonoBehaviour
{
    public enum TypeDrop
    {
        useIdSO,
        useSO,
        other
    }

    public TypeDrop type;

    [ConditionalField(nameof(type), false, TypeDrop.useIdSO)]
    public int idItemSO;
    [ConditionalField(nameof(type), false, TypeDrop.useIdSO)]
    public int count = 1;

    [ConditionalField(nameof(type), false, TypeDrop.useSO)]
    public ItemSO itemSO;

    private void Start()
    {
        if( type == TypeDrop.useSO)
        {
            idItemSO = itemSO.id;
        }
    }

    public void Off()
    {
        Destroy(gameObject);    
    }
}


