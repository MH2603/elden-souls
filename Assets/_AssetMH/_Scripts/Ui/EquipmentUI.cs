using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EquipmentUI : MonoBehaviour
{
    public List<SlotEpuipmentUI> listSlot;

    ItemCollected itemCollected;
    ItemSO itemSO;

    public void Init()
    {
        for (int i = 0; i < listSlot.Count; i++)
        {
            listSlot[i].Init();
        }
    }

    void UpdateData(int idCollected)
    {
        itemSO = null;
        itemCollected = null;

        itemCollected = DataManager.instance.GetItemCollected(idCollected);
        itemSO = DataManager.instance.GetDataItemSO(itemCollected.idSO);
    }

    public void CheckToEquip(int idCollected, bool ToEquip = true)
    {
        UpdateData(idCollected);    

        if (!itemSO)
        {
            Debug.Log("Ko tim thay item !!!");
            return;
        }

        for (int i = 0; i < listSlot.Count; i++)
        {
            if (listSlot[i].type == itemSO.type)
            {
                if (ToEquip)
                {
                    listSlot[i].CheckToEquipItem(itemSO);
                    Debug.Log("--> Equipment UI equip Item : " + idCollected + "--- Type : " + itemSO.type);
                }
                else
                {
                    listSlot[i].CheckToEquipItem(null);
                }
            }
        }
    }

}
