using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionItemUI : MonoBehaviour
{
    [Header("------------ Component ---------------")]
    public Button btnEquip;
    public Button btnUnEquip;
    public Button btnUse;
    public Button btnDrop;
    [Space]
    public RectTransform parent;
    public RectTransform content;
    [Space]
    InventoryUI inventoryUI;

    [Header("------------ Stats -------------------")]
    public Vector3 ofset;

    [SerializeField] bool isUnderCursor;

    [SerializeField] float ratioSave;
    Vector2 size;

    private void Awake()
    {
        //ratioSave = parent.sizeDelta.y - content.sizeDelta.y;    
    }

    public void Init(InventoryUI newInventoryUI)
    {
        inventoryUI = newInventoryUI;
        Off();
    }

    public void On(ItemUI itemUI)
    {

        if ( itemUI.itemSO.type != TypeItem.Attrition)
        {
            SelectEquipmentItem(DataManager.instance.GetItemCollected( itemUI.idCollected ));
        }

        parent.gameObject.SetActive(true);

        SetPos(itemUI.GetComponent<RectTransform>());
    }

    public void Off()
    {
        parent.gameObject.SetActive(false); 
        //parent.transform.parent = this.transform;
    }

    private void Update()
    {
        SetSizeParent();
        CheckToOff();
    }

    void SelectEquipmentItem(ItemCollected itemCollected)
    {
        OffBtn();

        //btnEquip.gameObject.SetActive(true);
        //btnDrop.gameObject.SetActive(true);
        switch (itemCollected.status)
        {
            case ItemCollected.StatusItem.CanUse:
                btnEquip.gameObject.SetActive(true);
                btnDrop.gameObject.SetActive(true);
                break;
            case ItemCollected.StatusItem.Equip:
                btnUnEquip.gameObject.SetActive(true);
                break;
        }


    }

    public void Set_IsUnderCursor(bool status)
    {
        isUnderCursor = status;
    }

    void CheckToOff()
    {
        if(Input.GetMouseButtonDown(0) && !isUnderCursor)
        {
            Off();
            inventoryUI.SetItemUiCr(null);
        }
    }


    void OffBtn()
    {
        btnEquip.gameObject.SetActive(false);
        btnUnEquip.gameObject.SetActive(false);
        btnUse.gameObject.SetActive(false);
        btnDrop.gameObject.SetActive(false);
    }

    void SetSizeParent()
    {
        size = parent.sizeDelta;
        size.y = content.sizeDelta.y + ratioSave;

        if (parent.sizeDelta == size) return;

        parent.sizeDelta = size;

        Debug.Log("Set Size");
    }

    void SetPos(RectTransform trans)
    {
        parent.position = trans.position + ofset;
    }
}
