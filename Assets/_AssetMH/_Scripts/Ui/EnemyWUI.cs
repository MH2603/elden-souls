using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using JetBrains.Annotations;
using UnityEngine.UI;
using TMPro;

public class EnemyWUI : MonoBehaviour
{
    [Separator("Hp")]
    public FillBase HpFill;
    public TextShowStat textDmg;

    [SerializeField]Camera cam;
    public void Init()
    {
        //cam = GameManager.instance.camMain;
        cam = Camera.main;
        HpSetFill(1);
    }

    public void Update()
    {
        LookAtCam(this.transform);
    }

    void LookAtCam(Transform trans)
    {
        trans.LookAt(trans.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);
    }

    public void HpSetFill(float ratio)
    {
        HpFill.SetFill(ratio);
    }

    public void ShowTextDmg(float dmg)
    {
        textDmg.Show(dmg);
    }

}
