﻿using UnityEngine;
using UnityEngine.UI;
using MyBox;
using System.Collections;

public class Gui : MonoBehaviour
{
    public GamePlayUI gamePlayUI;
    public InventoryUI inventoryUI;

    public void Init()
    {
        gamePlayUI.Init();
        inventoryUI.Init();
    }
}
