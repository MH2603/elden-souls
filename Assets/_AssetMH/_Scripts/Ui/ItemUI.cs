using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    [Header("* Status")]
    public ItemSO itemSO;
    public int idCollected;

    [Header("* Component")]
    public Image icon;
    public GameObject iconLock;
    public GameObject iconEquip;
    public Button btn;
    //[SerializeField] Canvas canvas;

    public void Init( ItemCollected itemCollected)
    {
        this.idCollected = itemCollected.idCollected;
        ItemSO newSO = DataManager.instance.GetDataItemSO(itemCollected.idSO);

        this.itemSO = newSO;   

        icon.sprite = itemSO.icon;

        //CheckUnlock();

        CheckStatus();

        btn.onClick.AddListener(() => SetItemCr());
    }

    public void CheckStatus()
    {
        CheckEquip(idCollected);
    }


    public void CheckUnlock()
    {
        if (DataManager.instance.playerData.level >= itemSO.levelCanUse
            && iconLock.activeInHierarchy)
        {
            iconLock.SetActive(false);
        }
        else if (DataManager.instance.playerData.level < itemSO.levelCanUse
            && !iconLock.activeInHierarchy)
        {
            iconLock.SetActive(true);
        }
    }

    public void CheckEquip(int idCollected)
    {
        if(DataManager.instance.GetItemCollected(idCollected).status 
            == ItemCollected.StatusItem.Equip)
        {
            iconEquip.SetActive(true);
        }
        else
        {
            iconEquip.SetActive(false);
        }

    }

    public void SetItemCr()
    {
        GameManager.Instance.gui.inventoryUI.SetItemUiCr(this); 
    }

}
