using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotEpuipmentUI : MonoBehaviour
{
    [Header("---------- Component ---------")]
    public Image icon;

    [Header("---------- Status ---------")]
    public ItemSO itemData;

    [Header("------------ Setup -------------")]
    public TypeItem type;
    

    public void Init()
    {
        //icon.gameObject.SetActive(false);
    }

    public void CheckToEquipItem(ItemSO newItemSO = null)
    {
        itemData = newItemSO;
        UpdateIcon();
    }

    void UpdateIcon()
    {
        if (itemData)
        {
            icon.sprite = itemData.icon;
            icon.gameObject.SetActive(true);
        }
        else
        {
            icon.sprite = null;
            icon.gameObject.SetActive(false);
        }
        
    }

    
}
