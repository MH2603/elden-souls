using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NotificationUI : MonoBehaviour
{
    public List<GameObject> listTutBtn;
    //public List<GameObject> listNotification;

    public Image icon;
    public TextMeshProUGUI textName;

    public float timeOff = 1f;


    public void FindedItemDrop() // call by playerCtrl
    {
        listTutBtn[0].SetActive(true); 
    }

    public void MissItemDrop()
    {
        listTutBtn[0].SetActive(false);
    }

    public void TakeItemDrop(ItemSO itemSO) // call by PlayerInventory
    {
        icon.transform.parent.gameObject.SetActive(true);
        MissItemDrop();
        icon.sprite = itemSO.icon;
        textName.text = itemSO.name;

        Invoke("OffInfor", timeOff);
    }

    void OffInfor()
    {
        icon.transform.parent.gameObject.SetActive(false);
    }


}
