using UnityEngine.UI;
using UnityEngine;

public class BtnChoseMenu : MonoBehaviour
{
    [Header("-------------- Setup by Hand ----------------------")]
    public TypeItem typeItem;
    public Button btnChose;
    public Transform bgSelected;


    public void OffSelected()
    {
        bgSelected.gameObject.SetActive(false);
    }

    public void OnSelected()
    {
        bgSelected.gameObject.SetActive(true);
    }

}
