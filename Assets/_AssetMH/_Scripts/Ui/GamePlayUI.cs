﻿using UnityEngine;
using UnityEngine.UI;
using MyBox;
using System.Collections;

public class GamePlayUI : MonoBehaviour
{
    [Separator("Component")]
    public PlayerStatUI playerStatUI;
    public NotificationUI notificationUI;

    public void Init()
    {
        playerStatUI.Init();  
    }
}
