using MyBox;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    //---------------- Item -----------------------------
    [Separator("Item")]

    [Header("Component")]
    public MenuItemUI menuUiPrefab;
    public Transform parentMenu;
    public List<BtnChoseMenu> listBtnChoseMenu;
    public List<MenuItemUI> listMenu;

    [Header("* Status")]
    public ItemUI itemUiCr;// item dang duoc chon


    [Header("---------------- Equipment -----------------------------")]
    public EquipmentUI equipmentUI;
    public ActionItemUI actionItem;

    
    [Header("------------------- Information Item----------------------")]
    public InformationItemUI inforItemUI;


    [Header("------------------------ Status Player ---------------------")]
    public StatusPlayerUI statusStatUi;

    #region( Init ) ---------------------------------------------------------
    public void Init()
    {

        equipmentUI.Init(); 
        statusStatUi.Init();
        inforItemUI.Init();

        LoadData_ItemCollected();

        actionItem.gameObject.SetActive(true);
        actionItem.Init(this);

        actionItem.btnEquip.onClick.AddListener(() => Equip());
        actionItem.btnUnEquip.onClick.AddListener(() => UnEquip());
    }

    void Init_ListBtnChoseMenu()
    {
        foreach( BtnChoseMenu btnChose in listBtnChoseMenu)
        {
            btnChose.btnChose.onClick.AddListener(() => SelectedMenu(btnChose.typeItem));
            btnChose.OffSelected();
        }
    }

    void LoadData_ItemCollected()
    {
        // Step 1
        Create_MenuAndItemUI();

        // Step 2
        Init_ListBtnChoseMenu();
    }


    void Create_MenuAndItemUI()
    {
        foreach (BtnChoseMenu btnChose in listBtnChoseMenu)
        {
            MenuItemUI newMenu = Instantiate(menuUiPrefab, parentMenu);
            newMenu.Init(btnChose.typeItem);
            listMenu.Add(newMenu);  
        }

        for (int i = 0; i < DataManager.instance.playerData.listItemCollected.Count; i++)
        {
            AddNewItem(DataManager.instance.playerData.listItemCollected[i].idCollected);
        }

        //SelectedMenu(TypeItem.Attrition);
    }

    #endregion

    public void AddNewItem(int idCollected)
    {
        
        ItemCollected newCollected = DataManager.instance.GetItemCollected(idCollected);
        ItemSO itemSO = DataManager.instance.GetDataItemSO(newCollected.idSO);

        for (int i=0; i < listMenu.Count; i++)
        {
            if (listMenu[i].typeItem == itemSO.type)
            {
                listMenu[i].AddNewItem(newCollected);
                return;
            }  
        }

        Debug.LogError("Ko tim dc Menu cung Type ???");
    }

    public void SelectedMenu(TypeItem type) // call by button
    {
        for (int i=0; i < listMenu.Count; i++)
        {
            listMenu[i].gameObject.SetActive(false);

            if (type == listMenu[i].typeItem)
            {
                listMenu[i].gameObject.SetActive(true);
            }
        }

        foreach (BtnChoseMenu btnChose in listBtnChoseMenu)
        {
            if (btnChose.typeItem == type)
            {
                btnChose.OnSelected();
            }
            else
            {
                btnChose.OffSelected();
            }
        }
    }


    void CallActionItem(bool status)
    {
        if (status)
        {
            actionItem.On(itemUiCr);
        }
        else
        {
            actionItem.Off();
        }
    }

    public void SetItemUiCr(ItemUI newItemUi = null)
    {
        if (newItemUi && newItemUi == itemUiCr) return;

        itemUiCr = newItemUi;  
        
        if( itemUiCr)
        {
            CallActionItem(true);
            inforItemUI.ShowInfo(itemUiCr.itemSO);
        }
        else
        {
            CallActionItem(false);
            inforItemUI.HideInfo();
        }
    }

    public void Equip()
    {
        if( !itemUiCr)
        {
            Debug.Log("Chua chon item UI nao !!!");
            return;
        }

        GameManager.Instance.player.inventory.LoadEquipment(itemUiCr.idCollected);
        UpdateStatusAllItem();

        SetItemUiCr(null);
    }
    public void UnEquip()
    {
        if (!itemUiCr)
        {
            Debug.Log("Chua chon item UI nao !!!");
            return;
        }

        GameManager.Instance.player.inventory.LoadEquipment(itemUiCr.idCollected, false);
        UpdateStatusAllItem();

        SetItemUiCr(null);

    }

    public void DropItem()
    {
        if (!itemUiCr)
        {
            Debug.Log("Chua chon item UI nao !!!");
            return;
        }

        ItemCollected itemCollected = DataManager.instance.GetItemCollected(itemUiCr.idCollected);

    }

    void UpdateStatusAllItem()
    {
        foreach (MenuItemUI menu in listMenu)
        {
            menu.UpdateStatusItem();
        }
    }
}
