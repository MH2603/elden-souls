using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class FlickerUI : MonoBehaviour
{
    public Image image;
    public bool isFlicker;
    [Header("------------ Setup ---------------------")]
    public float timePerFlicker = 1f;

    bool up;
    Color color;

    private void OnEnable()
    {
        if( !image ) image = GetComponent<Image>();

        isFlicker = true;
        StartCoroutine(Flicker());
    }

    private void OnDisable()
    {
        isFlicker = false;
        StopCoroutine(Flicker());
    }


    IEnumerator Flicker()
    {
        float num = 1;
        color = image.color;
        color.a = 1f;
        image.color = color;

        while (isFlicker) 
        {
            yield return new WaitForSeconds(0.02f);

            if( up)
            {
                num += 0.02f *(1/ timePerFlicker);
                if( num >= 1) up = false;    
            }
            else
            {
                num -= 0.02f * (1 / timePerFlicker);
                if (num <= 0) up = true;
            }

            color.a = num;
            image.color = color;
        }
    }

    
}
