using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MyBox;
using System.Collections;
using Unity.VisualScripting;

public class FillBase : MonoBehaviour
{
    [Separator("Component")]
    public GameObject parent;
    public Image fill;
    public Image fillFake;
    public RectTransform bar;
    [Separator("Stat")]
    
    public float timeRunToZero = 2f;
    public float timeDealayFakeRun = 1f;

    float ratioCurrent;
    float timeDelayCurrent;
    Vector2 size;
    Vector3 pos;

    private void Awake()
    {
        size = bar.sizeDelta;
        pos = bar.position;

    }

    private void Update()
    {
        CheckActive();
    }

    void CheckActive()
    {
        if (fill.fillAmount == 1 && parent.activeInHierarchy) parent.SetActive(false);
        if (fill.fillAmount < 1 && !parent.activeInHierarchy) parent.SetActive(true);
    }

    public void SetFill(float ratio)
    {
        ratioCurrent = ratio;   
        timeDelayCurrent = timeRunToZero * Mathf.Abs(fill.fillAmount - ratio);

        fill.DOFillAmount(ratio, timeRunToZero * Mathf.Abs(fill.fillAmount - ratio));

        CancelInvoke("SetFillFake");

        if (ratio < fill.fillAmount)
        {
            Invoke("SetFillFake", timeDealayFakeRun);
        }
        else
        {
            Invoke("SetFillFake" , 0f);
        }

    }

    void SetFillFake()
    {
        fillFake.DOKill();
        fillFake.DOFillAmount(ratioCurrent, timeDelayCurrent );
    }

    public void SetSizeBar(float ratio)
    {
        pos.x -= size.x * (1 - ratio) / 2;
        size.x *= ratio;

        bar.position = pos;
        bar.sizeDelta = size;
    }
}
