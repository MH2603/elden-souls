using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InformationItemUI : MonoBehaviour
{
    public Transform parent;

    public Image iconItem;
    public TextMeshProUGUI textName;
    [Space]
    public TextMeshProUGUI textDescribe;

    [Header("------------- TextStats ------------")]
    public TextMeshProUGUI textDmg;
    public TextMeshProUGUI textHp;
    public TextMeshProUGUI textSta;
    public TextMeshProUGUI textMana;
   
    public void Init()
    {
        parent.gameObject.SetActive(false); 
    }

    public void ShowInfo(ItemSO itemSO)
    {
        iconItem.sprite = itemSO.icon;  
        textName.text = itemSO.name;
        textDescribe.text = itemSO.describe;

        textDmg.text = "0";
        textHp.text = "0";
        textSta.text = "0";
        textMana.text = "0";

        EquipmentItemSO eqmItemSO = DataManager.instance.GetEquipmentItemSO(itemSO.id);

        if (eqmItemSO)
        {
            textDmg.text = eqmItemSO.statBase.dmgBase + "";
            textHp.text = eqmItemSO.statBase.hpBase + "";
            textSta.text = eqmItemSO.statBase.staBase + "";
            textMana.text = eqmItemSO.statBase.manaBase + "";
        }


        parent.gameObject.SetActive(true);
    }

    public void HideInfo()
    {
        parent.gameObject.SetActive(false); 
    }

}
