using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class StatusPlayerUI : MonoBehaviour
{
    [Header("Text Stats")]
    public TextMeshProUGUI textLevel;
    public TextMeshProUGUI textCoin;
    public TextMeshProUGUI textDmg;
    public TextMeshProUGUI textHp;
    public TextMeshProUGUI textStamina;
    public TextMeshProUGUI textMana;
    [Space]
    public TextMeshProUGUI textPointUpgrade;


    [Header("Btn Upgrade")]
    public Button btnDmg;
    public Button btnHp;
    public Button btnStamina;
    public Button btnMana;

    [Header("Component Status")]
    public PlayerStatManager statManager;

    
    public void Init()
    {
        statManager = GameManager.Instance.player.statManager;

        btnDmg.onClick.AddListener(() => UpgradeStats("Dmg") );
        btnHp.onClick.AddListener(() => UpgradeStats("MaxHp") );
        btnStamina.onClick.AddListener(() => UpgradeStats("MaxSta"));

        if (statManager.playerData.pointUpStats > 0) OnBtnUpgrade(true);
        else OnBtnUpgrade(false);   
    }

    private void Update()
    {
        if (statManager && gameObject.activeInHierarchy)
        {
            SetUp();
        }
    }

    public void SetUp()
    {
        textLevel.text = statManager.playerData.level.ToString();   
        textCoin.text = statManager.playerData.coin.ToString(); 

        textDmg.text = statManager.dmg.ToString();   
        textHp.text = statManager.currentHp.ToString() + " / " +  statManager.maxHP.ToString();
        textStamina.text = statManager.maxStamina.ToString();
        textMana.text = statManager.currentMana + " / " + statManager.maxMana;

        textPointUpgrade.text = statManager.playerData.pointUpStats.ToString();
    }

    public void UpLevel() // call by player when up level
    {
        OnBtnUpgrade(true);
    }

    void UpgradeStats(string nameStats)
    {
        if( !statManager )
        {
            Debug.LogError(" Tao ko tim thay Stat Manager ??? ");
            return;
        }

        float ratio = statManager.statSO.ratioPlus;

        statManager.playerData.pointUpStats--;

        switch (nameStats)
        {
            case "Dmg":
                statManager.UpdateDmg(ratio,true);
                break;
            case "MaxHp":
                statManager.UpdateHpMax(ratio,true);
                break;
            case "MaxSta":
                statManager.UpdateStaMax(ratio,true);
                break;
        }

        if(statManager.playerData.pointUpStats <= 0)
        {
            OnBtnUpgrade(false);
        }
    }

    void OnBtnUpgrade(bool status)
    {
        btnDmg.gameObject.SetActive(status);    
        btnHp.gameObject.SetActive(status);
        btnStamina.gameObject.SetActive(status);    
        btnMana.gameObject.SetActive(status);

        //textPointUpgrade.transform.gameObject.SetActive(status);
    }



}


