using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuItemUI : MonoBehaviour
{
    public TypeItem typeItem;

    [Header("------------- Asset ------------------")]
    public ItemUI itemUIPrefab;

    [Header("-------------- Component ---------------")]
    public Transform content;
    public List<ItemUI> listItemUI;


    public void Init(TypeItem typeItem)
    {
        this.typeItem = typeItem;
        name = "Menu-" + typeItem;
    }


    public void OnMenuItem(bool status)
    {
        content.gameObject.SetActive(status);    
    }

    public void AddNewItem(ItemCollected itemCollected)
    {
        ItemUI newItemUI = Instantiate( itemUIPrefab, content.position, Quaternion.identity, content);
        newItemUI.Init(itemCollected);

        listItemUI.Add(newItemUI);  
    }

    public void UpdateStatusItem()
    {
        foreach (ItemUI itemUI in listItemUI)
        {
            if (itemUI)
            {
                itemUI.CheckStatus();
            }
            else
            {
                listItemUI.Remove(itemUI);
            }
        }
    }

   
}
