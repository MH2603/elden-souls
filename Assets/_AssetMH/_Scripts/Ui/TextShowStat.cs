﻿
using UnityEngine;
using MyBox;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class TextShowStat : MonoBehaviour
{

    [Separator("Component")]
    public TextMeshProUGUI textUi;

    [Separator("Stat")]
    public float timeShow = 3f;
    //public Vector3 scale = new Vector3(1.2f, 1.2f, 1.2f);
    //public float timeScale = 0.5f;
    //public Color colorText;

    public void Show(float stat)
    {
        textUi.gameObject.SetActive(true);  
        textUi.text = stat.ToString();

        //textUi.DOKill();    
        //textUi.transform.DOScale(scale, timeScale).SetEase(Ease.InBack) ;
        //textUi.DOColor(colorText, timeScale).SetEase(Ease.InBack);

        CancelInvoke("Hide");
        Invoke("Hide", timeShow);
    }

    void Hide()
    {
        textUi.gameObject.SetActive(false);
        textUi.text = "";
    }


}