using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCvGroup : MonoBehaviour
{
    public CanvasGroup cvGroup;
    public enum Type
    {
        OnEnable,
        CallFun
    }

    [Header("------------- Setup ------------")]
    public Type type;   
    public float timeShow = 1f;


    public void OnEnable()
    {
        if( !cvGroup ) cvGroup = GetComponent<CanvasGroup>();

        if (type == Type.OnEnable) CallShow();
        
    }

    public void CallShow() // call by other sripts
    {
        StartCoroutine(ShowCanvaGroup());
    }

    IEnumerator ShowCanvaGroup()
    {
        cvGroup.alpha = 0f;

        for (int i=0; i < timeShow/0.02f; i++)
        {
            yield return new WaitForSeconds(0.02f);

            cvGroup.alpha += 0.02f / timeShow;
        }

        cvGroup.alpha = 1f; 
    }
}


