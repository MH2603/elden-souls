﻿using UnityEngine;
using UnityEngine.UI;
using MyBox;
using System.Collections;
using TMPro;
using DG.Tweening;
using Unity.VisualScripting;

public class PlayerStatUI : MonoBehaviour
{
    [Separator("Componet")]
    public FillBase Hp; 
    public FillBase Stamina;
    public FillBase Mana;
    [Space]
    public TextMeshProUGUI textLevel;
    public Image fillExp;

    public void Init()
    {
        HpSetFill(1);
        StaminaSetFill(1);  
        ManaSetFill(1);
    }

    public void HpSetFill(float ratio)
    {
        Hp.SetFill(ratio);
    }

    public void StaminaSetFill( float ratio)
    {
        Stamina.SetFill(ratio);
    }

    public void ManaSetFill(float ratio)
    {
        Mana.SetFill(ratio);
    }

    public void BarHpSetSize(float ratio)
    {
        Hp.SetSizeBar(ratio);   
    }

    public void BarStaSetSize(float ratio)
    {
        Stamina.SetSizeBar(ratio);
    }

    public void BarManaSetSize(float ratio)
    {
        Mana.SetSizeBar(ratio);
    }

    public void SetStatusLevel(float ratio)
    {
        textLevel.text = ( DataManager.instance.playerData.level + 1).ToString()  ;
        fillExp.fillAmount = ratio;
    }


}
