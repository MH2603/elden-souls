using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using MyBox;
using StarterAssets;
using UnityEngine.Events;

public class AnimationController : StateMachineBehaviour
{


    [Separator("Events at Enter Animation")]
    public bool UseEventEnter;
    [ConditionalField(nameof(UseEventEnter))] public ParentParameter ParameterEnter;


    [Separator("Events at Exit Animation")]
    public bool UseEventExit;
    [ConditionalField(nameof(UseEventExit))] public ParentParameter ParameterExit;

    private void OnEnable()
    {

    }

    void SetParameter(ParentParameter parent, Animator anim)
    {
        for (int i = 0; i < parent.arrayBool.Length; i++) // Set Bool
        {
            anim.SetBool(parent.arrayBool[i].name, parent.arrayBool[i].status);
        }

        for (int i = 0; i < parent.arrayInt.Length; i++) // Set Int
        {
            anim.SetInteger(parent.arrayInt[i].name, parent.arrayInt[i].value);
        }

        for (int i = 0; i < parent.arrayFloat.Length; i++) // Set Float
        {
            anim.SetFloat(parent.arrayFloat[i].name, parent.arrayFloat[i].value);
        }

        for (int i = 0; i < parent.arrayTrigger.Length; i++) // Set Trigger
        {
            anim.SetTrigger(parent.arrayTrigger[i].name);
        }
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (UseEventEnter)
        {
            SetParameter(ParameterEnter, animator);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (UseEventExit)
        {
            SetParameter(ParameterExit, animator);
        }

    }

}
[Serializable]
public struct ParentParameter
{
    public boolParameter[] arrayBool;
    public intParameter[] arrayInt;
    public floatParameter[] arrayFloat;
    public triggerParameter[] arrayTrigger; 
}

[Serializable]
public struct boolParameter
{
    public string name;
    public bool status;
}

[Serializable]
public struct intParameter
{
    public string name;
    public int value;
}

[Serializable]
public struct floatParameter
{
    public string name;
    public float value;
}

[Serializable]
public struct triggerParameter
{
    public string name;
}