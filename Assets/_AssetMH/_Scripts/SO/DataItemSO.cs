using MyBox;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/ItemSO/DataItemSO")]
public class DataItemSO : ScriptableObject
{
    public ItemSO[] listItemCanUse;
    public EquipmentItemSO[] listEquipment;
    public WeaponItemSO[] listWeapon;
    public ClotherItemSO[] listClother;
    [Space]
    public ItemSO[] listItems;

    int idStart;

    [ButtonMethod]
    public void SetUpListItem() // muc dich duy nhat la Set Up id cho tat ca Item
    {

        int lenght = listItemCanUse.Length + listWeapon.Length + listClother.Length /*+ listEquipment.Length*/;
        listItems = new ItemSO[lenght];

        AddArrayListItems(listItemCanUse);
        AddArrayListItems(listWeapon);
        AddArrayListItems(listClother);
        
        // -------------------- Data for Equipment -------------------
        AddArrayListEquipment();

        Debug.Log("Complete Set Up list Data");

    }

    void AddArrayListItems(ItemSO[] array)
    {

        for (int i = 0; i < listItems.Length; i++)
        {
            if (!listItems[i])
            {
                idStart = i;
                break;
            }
        }

        for (int i = 0; i < array.Length; i++)
        {
            listItems[idStart] = array[i];
            listItems[idStart].id = idStart;
            idStart++;
        }
    }

    void AddArrayListEquipment()
    {
        int lenghtOfEquipment = listWeapon.Length + listClother.Length;
        listEquipment = new EquipmentItemSO[lenghtOfEquipment];

        for (int i=0; i < listEquipment.Length; i++)
        {
            if(listWeapon.Length > i) listEquipment[i] = listWeapon[i];
            else listEquipment[i] = listClother[i - listWeapon.Length];
        }
    }
}
