using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/PrefabSO")]
public class PrefabsSO : ScriptableObject
{
    public TrailReward trailRewardPrefab;
}
