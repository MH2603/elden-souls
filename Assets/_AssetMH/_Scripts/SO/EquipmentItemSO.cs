using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/ItemSO/EquipmentItemSO")]
public class EquipmentItemSO : ItemSO
{
    public StatEquipment statBase;
}
