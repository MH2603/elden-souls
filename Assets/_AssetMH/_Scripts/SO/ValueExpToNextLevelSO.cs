using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/ExpToNextLevel")]
public class ValueExpToNextLevelSO : ScriptableObject
{
    public List<int> listValueExp;
}
    