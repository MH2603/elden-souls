using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/ItemSO")]
public class ItemSO : ScriptableObject
{
    public int id;
    public string name;
    public Sprite icon;
    public int levelCanUse;
    public TypeItem type;
    public string describe;
    public int countMax;
    public GameObject prefab;
}
    
public enum TypeItem
{
    None,
    Attrition,
    Weapon,
    Hat,
    Armor,
    Pants,
    Boots,
    Hand
}

[System.Serializable]
public struct StatEquipment
{
    public float dmgBase;
    public float hpBase;
    public float staBase;
    public float manaBase;
}