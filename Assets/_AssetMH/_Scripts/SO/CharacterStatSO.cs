using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/CharacterStatSO")]
public class CharacterStatSO : ScriptableObject
{
    public float dmgBase;
    public float hpBase;
    public float speedBase;
}
