using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/EnemyStatSO")]
public class EnemyStatSO : ScriptableObject
{
    [Separator("Stats")]
    [Header("* Body Stat")]
    public float dmg = 1f;
    public float maxHP = 100f;
    public float speedRun = 2f;

    [Header("* Check ")]
    public float angleCheck = 120f;
    public float distanceCheck = 5f;
    public LayerMask layerOb;
    [Space]
    public float distanceToAttack = 3f;
    public float distanceToMiss = 10f;
    [Space]
    public Vector2 distanceRandomIdle = new Vector2(1, 4);
    public Vector2 timeRandomIdle = new Vector2(1, 3);
    public float speedIdle = 1f;
}
