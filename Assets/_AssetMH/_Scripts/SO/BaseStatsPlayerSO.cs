using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/PlayerStatSO")]
public class BaseStatsPlayerSO : ScriptableObject
{
    public float dmgBase;
    [Header("Hp")]
    public float hpBase;
    [Header("Stamina")]
    public float staminaBase;
    public float speedUpSta;
    public float delayUpSta = 2f;
    [Header("Mana")]
    public float manaBase;

    [Header("*** Level")]
    public int countPointPlus; // so luong diem Upgrade nhan duoc khi Up Level
    public float ratioPlus; // ty le Stats duoc Upgrade khi nang diem cho 1 chi so

    [Header("Other")]
    public float radiusCheckDrop;
}
