using MyBox;
using UnityEngine;

public class TrailReward : MonoBehaviour
{
    [Separator("Component")]
    public MoveToTarget movement;
    private PlayerController playerController;  

    [Separator("Stat")]
    public int coinReward;
    public int expReward;

    private void OnEnable()
    {
        
    }

    public void Init(int coin, int exp)
    {
        if (playerController == null)
        {
            playerController = GameManager.Instance.player;
        }

        coinReward = coin;
        expReward = exp;

        movement.CompleteEvent.AddListener(RewardForPlayer);
        movement.Init(playerController.posHeart);
    }

    public void RewardForPlayer()
    {
        if( playerController != null)
        {
            playerController.GetCoinAndExp(coinReward, expReward);
            Destroy(gameObject);
        }
    }
}
