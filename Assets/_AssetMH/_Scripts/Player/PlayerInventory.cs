﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Unity.VisualScripting;

public class PlayerInventory : MonoBehaviour
{
    PlayerController playerCtrl;

    [Header("---------------------- Weapon ------------------------")]
    public WeaponSlotManager weaponSlotManager;


    [Header("-------------------- Clother -------------------------")]
    public ClotherSlotsManager hatSlotManager;
    public ClotherSlotsManager armorSlotManager;
    public ClotherSlotsManager handSlotManager;
    public ClotherSlotsManager pantSlotManager;
    public ClotherSlotsManager bootSlotManager;

    public void Init(PlayerController newPlayer)
    {
        playerCtrl = newPlayer;

        LoadEquipmentFromCollection();
    }

    void LoadEquipmentFromCollection()
    {
        foreach (ItemCollected itemCollected in DataManager.instance.playerData.listItemCollected)
        {
            if (itemCollected.status == ItemCollected.StatusItem.Equip)
            {
                LoadEquipment(itemCollected.idCollected);
            }
        }
    }

    public void LoadEquipment(int idCollected, bool ToEquip = true)
    {
        if (idCollected < 0) return;

        ItemCollected itemCollected = DataManager.instance.GetItemCollected(idCollected);
        ItemSO item = DataManager.instance.GetItemSO_ByIdCollected(idCollected);

        if (!item)
        {
            return;
        }

        switch (item.type)
        {
            case TypeItem.Weapon:
                Call_LoadWeapon(idCollected, ToEquip);
                break;

            case TypeItem.Hat:
                if(ToEquip) LoadHat(itemCollected);
                else LoadHat(null);
                break;

            case TypeItem.Armor:
                if(ToEquip) LoadArmor(itemCollected);  
                else LoadArmor(null);   
                break;

            case TypeItem.Hand:
                if(ToEquip) LoadHand(itemCollected);
                else LoadHand(null);
                break;

            case TypeItem.Pants:
                if(ToEquip) LoadPants(itemCollected);   
                else LoadPants(null);
                break;

            case TypeItem.Boots:
                if(ToEquip) LoadBoots(itemCollected);
                else LoadBoots(null);   
                break;

        }

        DataManager.instance.CheckToEquipItem(itemCollected.idCollected, ToEquip);
        GameManager.Instance.gui.inventoryUI.equipmentUI.CheckToEquip(idCollected, ToEquip);

    }

    
    #region( Weapon ) -----------------------------------------------

    void Call_LoadWeapon(int idCollected,bool isEquip)
    {
        if (isEquip)
        {
            LoadWeapon(idCollected);

            playerCtrl.locomotor.LoadWeapon();
        }
        else
        {
            UnEquipWeaponCr();
        }
    }

    void UnEquipWeaponCr()
    {
        weaponSlotManager.LoadWeapon(null);
    }

    public void EventLoadWeapon() // call by event animaion
    {
        weaponSlotManager.OnActiveWeapon();
    }


    void LoadWeapon(int idWeaponCr) 
    {

        ItemCollected weaponCollected = DataManager.instance.GetItemCollected(idWeaponCr);

        if (weaponCollected != null)
        {
            weaponSlotManager.LoadWeapon(weaponCollected);
        }
        else
        {
            Debug.LogError(" Co loi trong viec tim weapon collected ");
        }
        
    }

    public bool IsHaveWeapon() 
    {
        if (weaponSlotManager.dmgColliderCr) return true;
        else return false;
    }

    #endregion

    #region( Clothers ) -----------------------------------------------------
    void LoadHat(ItemCollected itemCollected) // call by method "LoadEquipment(int idCollected)"
    {
        hatSlotManager.LoadClother(itemCollected);
        
    }

    void LoadArmor(ItemCollected itemCollected) // call by method "LoadEquipment(int idCollected)"
    {
        armorSlotManager.LoadClother(itemCollected);  
    }

    void LoadHand(ItemCollected itemCollected)
    {
        handSlotManager.LoadClother(itemCollected);
    }

    void LoadPants(ItemCollected itemCollected)
    {
        pantSlotManager.LoadClother(itemCollected);
    }

    void LoadBoots(ItemCollected itemCollected)
    {
        bootSlotManager.LoadClother(itemCollected);  
    }

    #endregion

}
