using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Unity.VisualScripting;
using JetBrains.Annotations;

public class PlayerStatManager : MonoBehaviour
{
    [Separator("Componet")]
    public PlayerStatUI statUI;

    [Separator("Asset")]
    public BaseStatsPlayerSO statSO;

    [Separator("Stat")]
    public float maxHP;
    public float maxStamina;
    public float speedUpSta;
    public float delayUpSta = 2f;
    public float maxMana;
    public float dmg;
    public float speed;

    [Separator("Status Stats")]
    public PlayerData playerData;
    public float currentHp { get; private set; }
    public float currentStamina { get; private set; }   
    public float currentMana { get; private set; }

    
    
   
    public void Init()
    {
        statUI = GameManager.Instance.gui.gamePlayUI.playerStatUI;
        DataManager.instance.CheckNextLevel();

        LoadStats();
    }

    void LoadStats()
    {

        playerData = DataManager.instance.playerData;

        dmg = statSO.dmgBase * ( 1 + playerData.RatioDmgPlus );

        maxHP = statSO.hpBase * ( 1 + playerData.RatioHpPlus );

        maxStamina = statSO.staminaBase * ( 1 + playerData.RatioStaPlus ) ;
        speedUpSta = statSO.speedUpSta;
        delayUpSta = statSO .delayUpSta;    

        maxMana = statSO.manaBase * ( 1 +  playerData.RatioManaPlus );

        currentHp = maxHP;
        currentStamina = maxStamina;
        currentMana = maxMana;
    }

    #region( Set Current Stats)
    public void SetHpCurrent(float value = 0f)
    {
        currentHp += value;
        statUI.HpSetFill( currentHp/ maxHP);
    }

    public void SetStaCurrent(float value = 0f)
    {
        currentStamina += value;
        statUI.StaminaSetFill(currentStamina / maxStamina);

        if( value < 0 )
        {
            CancelInvoke("UpStaPerSecond");
            Invoke("UpStaPerSecond", delayUpSta);
        }
    }

    void UpStaPerSecond()
    {
        Invoke("UpStaPerSecond", 0.1f);

        if( currentStamina >= maxStamina)
        {
            currentStamina = maxStamina;
            return;
        }
        else
        {
            currentStamina += speedUpSta;
            statUI.StaminaSetFill(currentStamina/ maxStamina);
        }
    }

    public void SetManaCurrent(float value = 0f)
    {
        currentMana += value;
        statUI.ManaSetFill(currentMana / maxMana);
    }

    #endregion

    public void UpdateDmg( float ratioPlus = 0 , bool save = false)
    {
        float ratioUpdate = 1 + playerData.RatioDmgPlus + ratioPlus;
        dmg = statSO.dmgBase * ratioUpdate; 

        if( save)
        {
            playerData.RatioDmgPlus += ratioPlus;
            DataManager.instance.SavePlayerData();
        }
    }

    public void UpdateHpMax( float ratioPlus = 0, bool save = false)
    {
        float curretHpRatio = currentHp/ maxHP;
        float ratioUpdate = 1 + playerData.RatioHpPlus + ratioPlus;

        if ( save )
        {
            playerData.RatioHpPlus += ratioPlus;
            DataManager.instance.SavePlayerData();
        }

        maxHP = statSO.hpBase * ratioUpdate;

        currentHp = curretHpRatio * maxHP;

        statUI.BarHpSetSize(ratioUpdate);
    }

    public void UpdateStaMax(float ratioPlus = 0, bool save = false)
    {
        float curretStaRatio = currentStamina / maxStamina;
        float ratioUpdate = 1 + playerData.RatioStaPlus + ratioPlus;

        if (save)
        {
            playerData.RatioStaPlus += ratioPlus;
            DataManager.instance.SavePlayerData();
        }

        maxStamina = statSO.staminaBase * ratioUpdate;

        currentStamina = curretStaRatio * maxStamina;

        statUI.BarStaSetSize(ratioUpdate);
    }



  
}

