﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class ClotherSlotsManager : MonoBehaviour
{
    [Header("------------- Setup By Hand ----------------")]
    public ClotherDataManager clotherDataDefault;
    public List<PartClotherSlot> listPartClotherSlot;


    [Header("------------ Auto --------------")]
    public ItemCollected itemCollected;
    public ClotherItemSO clotherSOCr;
    public ClotherDataManager clotherDataCr;

    public void LoadClother(ItemCollected itemCollected = null)
    {
        if (this.itemCollected.idSO >= 0) // đưa Euipment đang dùng lúc trước thành status CanUse
        {
            DataManager.instance.SetStatusItem(this.itemCollected.idCollected, ItemCollected.StatusItem.CanUse);
        }

        if (clotherDataCr && clotherDataCr.gameObject) Destroy(clotherDataCr.gameObject); // xóa đi GameObject data của clother trước

        if (itemCollected == null)
        {
            Debug.Log("--> Load Clother Default");
            UpdateToClotherDefault();
            return;
        }

        this.itemCollected = itemCollected;

        clotherSOCr = DataManager.instance.GetDataClother(itemCollected.idSO);
        if (!clotherSOCr) return;

        if (clotherSOCr.prefab.TryGetComponent<ClotherDataManager>(out ClotherDataManager clotherData))
        {
            CreateClotherData(clotherData);

            UpdatePartSlot();
        }
        else
        {
            Debug.LogError(" Prefabs cua itemSO ko chua component DataClother !!!");
        }
        
    }

    void CreateClotherData(ClotherDataManager clotherData)
    {
        clotherDataCr = Instantiate(clotherData);
        clotherDataCr.transform.SetParent(this.transform, false);
    }

    private void UpdateToClotherDefault()
    {
        if (clotherDataDefault)
        {
            CreateClotherData(clotherDataDefault);
            UpdatePartSlot();
        }
    }


    #region ( Load Data to Load Slot)
    [ButtonMethod]
    void LoadPartOfDataClother()
    {
        for(int i=0; i< clotherDataCr.listClotherData.Count; i++)
        {
            FindPartSameType(clotherDataCr.listClotherData[i]);
        }

    }

    void FindPartSameType(PartClotherData newPartData)
    {
        for (int i=0; i < listPartClotherSlot.Count; i++)
        {
            if (newPartData.typePart == listPartClotherSlot[i].typePart)
            {
                ChangeMeshSkinnerOfPart(newPartData, listPartClotherSlot[i]);
                return;
            }
        }

        Debug.LogError("Ko tim duoc PartClotherSlot cung Type ???");
    }
    #endregion

    #region( Load Slot to load Data )
    void UpdatePartSlot()
    {
        foreach(PartClotherSlot slot in listPartClotherSlot)
        {
            FindPartClotherDataSameType(slot);
        }
    }

    void FindPartClotherDataSameType(PartClotherSlot slot)
    {
        foreach(PartClotherData partData in clotherDataCr.listClotherData)
        {
            if( partData.typePart == slot.typePart)
            {
                ChangeMeshSkinnerOfPart(partData,slot);
                return;
            }
        }

        slot.skinnerOfPart.gameObject.SetActive(false);
    }
    #endregion

    void ChangeMeshSkinnerOfPart(PartClotherData newPartData, PartClotherSlot slot)
    {
        slot.skinnerOfPart.sharedMesh = newPartData.mesh;
        slot.skinnerOfPart.gameObject.SetActive(true);
    }
}


[System.Serializable]
public struct PartClotherSlot
{
    public PartClother typePart;
    public SkinnedMeshRenderer skinnerOfPart;
}


public enum PartClother
{
    //-------------- Hat ----------------
    Head,
    Hat,

    //-------- Armor ---------------
    Torso,
    Shoulder_Left,
    Shoulder_Right,

    //---------------- Hand -----------------
    ArmUp_Left,
    ArmUp_Right,
    ArmLow_Left,
    ArmLow_Right,
    Hand_Left,
    Hand_Right, 

    //------------------ Pants --------------
    Hips,
    
    //------------ Boots ---- 
    Leg_Left,
    Leg_Right,
}