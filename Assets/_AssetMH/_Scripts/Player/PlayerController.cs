using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using static UnityEditor.Progress;

public class PlayerController : CharactersController
{
    [Separator("Component")]
    public PlayerLocomotor locomotor;
    public PlayerStatManager statManager;
    public PlayerInventory inventory;
    public VfxManger vfxManger;

    public GameObject posHeart;

    [Header("* Ui")]
    public Gui gui;

    [Separator("Input")]
    public KeyCode keyRoll;

    [Separator("Setting")]
    public float cdRoll = 1f;
    public LayerMask layerItemDrop;
    public KeyCode keyTakeItem = KeyCode.E;

    [Separator("Status")]
    public bool NoDMG;
    public ItemDrop itemDropCr;

    //public void Start()
    //{
    //    Init(); 
    //}

    public void Init()
    {
        statManager.Init(); // load data
        inventory.Init(this);

        locomotor.Init(this);
    }

    public void Update()
    {
        if(Input.GetKeyDown(keyRoll))
        {
            Roll();
        }

        CheckItemDrop();
    }

    public override void TakeDamage(float dmg)
    { 
        //if( NoDMG ) return; 

        if (statManager.currentHp <= 0) return;

        statManager.SetHpCurrent(-dmg);

        if( statManager.currentHp <= 0)
        {
            Debug.Log(this.name + "---> Die !!!");
        }
    }

    public void Roll()
    {
        if ( /*locomotor.IsAniAttacking() ||*/ !locomotor.isGrounded || locomotor.IsAniRolling() || !UseStamina(10)) return;
        else
        {
            locomotor.Roll();
        }
     
    }


    public bool UseStamina(float amountSta)
    {
        if( statManager.currentStamina >= amountSta)
        {
            statManager.SetStaCurrent(-amountSta);
            return true;
        }
        else
        {
            Debug.Log("--> Dont enough Stamina");
            return false;
        }
      
    }

    public void OnNoDMG()
    {
        NoDMG = true;
    }

    public void OffNoDMG()
    {
        NoDMG = false;
    }

    public void GetCoinAndExp(int coin, int exp)
    {
        DataManager.instance.GetCoinAndExp(coin, exp);

        vfxManger.GetReward();
    }

    public void SetExp(float ratio, bool upLevel = false) // call by DataManager 
    {
        gui.gamePlayUI.playerStatUI.SetStatusLevel(ratio);

        if( upLevel)
        {
            vfxManger.UpLevel(); 
            gui.inventoryUI.statusStatUi.UpLevel();
        }
    }

    #region( Find and Take Item Drop) //-------------------------------------------------------

    void CheckItemDrop()
    {

        if (itemDropCr)
        {

            if (Input.GetKeyDown(keyTakeItem))
            {
                TakeItemDrop();
            }

            if (itemDropCr &&
                Vector3.Distance(this.transform.position, itemDropCr.transform.position) > statManager.statSO.radiusCheckDrop)
            {
                itemDropCr = null;
                gui.gamePlayUI.notificationUI.MissItemDrop();
            }

            return;
        }

        Collider[] hit = Physics.OverlapSphere(this.transform.position, statManager.statSO.radiusCheckDrop, layerItemDrop);

        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].GetComponent<ItemDrop>())
            {
                itemDropCr = hit[i].GetComponent<ItemDrop>();
                FindedItemDrop();
                return;
            }
        }

        gui.gamePlayUI.notificationUI.MissItemDrop();
    }
    void FindedItemDrop()
    {
        gui.gamePlayUI.notificationUI.FindedItemDrop(); 
    }

    void TakeItemDrop()
    {
        ItemCollected newCollected = DataManager.instance.AddItemCollectionToData(itemDropCr.idItemSO, itemDropCr.count);
        gui.inventoryUI.AddNewItem(newCollected.idCollected);

        GameManager.Instance.gui.gamePlayUI.notificationUI.TakeItemDrop(DataManager.instance.GetDataItemSO(itemDropCr.idItemSO));

        Destroy(itemDropCr.gameObject);
        itemDropCr = null;
    }



    #endregion
}
