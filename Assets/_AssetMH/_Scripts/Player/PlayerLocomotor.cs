using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using JetBrains.Annotations;

public class PlayerLocomotor : MonoBehaviour
{
    CharacterController controller;
    public Animator anim;
    Transform cam;
    [Separator("Name Animation")]
    //public List<string> listNameAni;
    public bool showParamater;
    [ConditionalField(nameof(showParamater))] public string prMovement;
    [ConditionalField(nameof(showParamater))] public string prHorizontal;
    [ConditionalField(nameof(showParamater))] public string prVertical;
    [ConditionalField(nameof(showParamater))] public string prLockOn;
    [ConditionalField(nameof(showParamater))] public string prAttackStep;
    [ConditionalField(nameof(showParamater))] public string prAttacking;
    [ConditionalField(nameof(showParamater))] public string prRoll;
    [ConditionalField(nameof(showParamater))] public string prStateCurrent;
    [ConditionalField(nameof(showParamater))] public string prIsAttack;
    [ConditionalField(nameof(showParamater))] public string prLoadWeapon;

    [Separator("Movement")]
    [SerializeField] float speedSmoothVelocity;
    float speedSmoothTime;
    float currentSpeed;

    Vector3 moveInput;
    Vector3 dir;

    [SerializeField] float moveSpeed = 2f;
    [SerializeField] float rotateSpeed = 3f;

    [Header("* Gravity & VectorY ")]
    public bool isGrounded;
    [SerializeField] float gravity = 25f;
    [SerializeField] float distanceCheckGr = 3f;
    [SerializeField] Transform posCheckGround;
    [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
    public float FallTimeout = 0.15f;
    private float _fallTimeoutDelta;
    [SerializeField] LayerMask groundLayer;
    RaycastHit hitGround;
    float velocityY;

    [Header("* Status")]
    public bool lockMovement;

    [Separator("Attack")]
    public KeyCode keyAttack;
    public float cooldownTime = 2f;
    public int noOfClicks = 0;
    float lastClickedTime = 0;
    float maxComboDelay = 1;

    PlayerController playerCtrl;

    void Start()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>(); 
        cam = Camera.main.transform;

        _fallTimeoutDelta = FallTimeout;
    }

    public void Init(PlayerController playerCtrl)
    {
        this.playerCtrl = playerCtrl;
    }

    void Update()
    {
        GetInput();
        PlayerMovement();
        if(!lockMovement) PlayerRotation();

        //Attack
        CheckAttack();

        anim.SetFloat(prStateCurrent, anim.GetCurrentAnimatorStateInfo(0).normalizedTime);
    }

    private void GetInput(){

        moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        Vector3 forward = cam.forward;
        Vector3 right = cam.right;
        forward.y = 0;
        right.y = 0;
        forward.Normalize();
        right.Normalize();

        dir = (forward * moveInput.y + right * moveInput.x).normalized;

        if ( IsAniAttacking()
             || IsAniRolling() )
        {
            dir = Vector3.zero;
        }
    }

    private void PlayerMovement(){

        currentSpeed = Mathf.SmoothDamp(currentSpeed, moveSpeed * dir.sqrMagnitude, ref speedSmoothVelocity, speedSmoothTime * Time.deltaTime);

        //// Gravity
        //if(velocityY > -10) velocityY -=  Time.deltaTime * gravity;
        GroudnAndGravity();

        Vector3 velocity = (dir  * currentSpeed) + Vector3.up * velocityY;

        controller.Move(velocity * Time.deltaTime);

        anim.SetFloat(prMovement, dir.magnitude, 0.1f, Time.deltaTime);
        anim.SetFloat(prHorizontal, moveInput.x, 0.1f, Time.deltaTime);
        anim.SetFloat(prVertical, moveInput.y, 0.1f, Time.deltaTime);
    }

    private void PlayerRotation()
    {
        if(dir.magnitude == 0) return;
        Vector3 rotDir = new Vector3(dir.x, dir.y, dir.z);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotDir), Time.deltaTime * rotateSpeed);
    }

    #region Ground and Gravity
    public void CheckGround()
    {
        //if( Physics.Raycast(posCheckGround.position,Vector3.down , out hitGround , distanceCheckGr, groundLayer ))
        //{
        //    if( hitGround .transform != null)
        //    {
        //        isGrounded = true;
        //    }
        //}

        //Raycast no check mesh collider
        //isGrounded = Physics.Raycast(posCheckGround.position, Vector3.down, out hitGround, distanceCheckGr, groundLayer);

        isGrounded = Physics.CheckSphere(posCheckGround.position, distanceCheckGr, groundLayer, QueryTriggerInteraction.Ignore);
    }

    public void GroudnAndGravity()
    {
        CheckGround();

        if (isGrounded)
        {
            _fallTimeoutDelta = FallTimeout;

            if (velocityY < 0.0f)
            {
                velocityY = -2f;
            }
        }
        else
        {
  
            // fall timeout
            if (_fallTimeoutDelta >= 0.0f)
            {
                _fallTimeoutDelta -= Time.deltaTime;
            }
            else
            {
                // update animator if using character
                
            }

        }

        // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
        velocityY -= gravity * Time.deltaTime;
    }


    #endregion  

    public void SetStatusLockOn(bool status )
    {
        if( status)
        {
            lockMovement = true;
            anim.SetBool(prLockOn, true);
        }
        else 
        { 
            lockMovement = false;
            anim.SetBool(prLockOn, false);
        }
    }

    #region Attack
    void CheckAttack()
    {
        if (Time.time - lastClickedTime > maxComboDelay)
        {
            noOfClicks = 0;
        }

        anim.ResetTrigger(prIsAttack);


        if (Input.GetMouseButtonDown(0) && !IsAniRolling() && isGrounded 
            && playerCtrl.inventory.IsHaveWeapon()  )
        {
            //OnClickAttack();
            anim.SetTrigger(prIsAttack);
            Debug.Log("Click Attack !!!");
        }

        
        CheckToOffAnima();
    }

    void OnClickAttack()
    {
       
        //so it looks at how many clicks have been made and if one animation has finished playing starts another one.
        lastClickedTime = Time.time;
        noOfClicks++;
        if (noOfClicks == 1)
        {
            anim.SetInteger(prAttackStep, 1);
            
            return;
        }
        noOfClicks = Mathf.Clamp(noOfClicks, 0, 3);

        if (noOfClicks >= 2 && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f 
            && anim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.9f
            && anim.GetCurrentAnimatorStateInfo(0).IsName("Attack01"))
        {
            anim.SetInteger(prAttackStep, 2);

            return;

        }
        if (noOfClicks >= 3 && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f 
            && anim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.9f
            && anim.GetCurrentAnimatorStateInfo(0).IsName("Attack02"))
        {
            anim.SetInteger(prAttackStep, 3);
           
            return;
        }
    }

    void CheckToOffAnima()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.9f && anim.GetCurrentAnimatorStateInfo(0).IsName("Attack01") 
            && anim.GetInteger(prAttackStep) == 1 )
        {
            anim.SetInteger(prAttackStep, 0);
        }
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.9f && anim.GetCurrentAnimatorStateInfo(0).IsName("Attack02") 
            && anim.GetInteger(prAttackStep) == 2 )
        {
            anim.SetInteger(prAttackStep, 0);
        }
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.9f && anim.GetCurrentAnimatorStateInfo(0).IsName("Attack03")
            && anim.GetInteger(prAttackStep) == 3 )
        {
            anim.SetInteger(prAttackStep, 0);
            noOfClicks = 0;
        }
    }

    public bool IsAniAttacking()
    {
        return anim.GetBool(prAttacking);
    }
    #endregion

    #region Roll

    public bool IsAniRolling()
    {
        if(anim.GetInteger(prRoll) == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void Roll()
    {
        if( dir.sqrMagnitude == 0)
        {
            anim.SetInteger(prRoll, -1);
        }
        else
        {
            anim.SetInteger(prRoll, 1);
        }
        
    }
    #endregion


    public void LoadWeapon()
    {
        anim.SetTrigger(prLoadWeapon);
    }

}
