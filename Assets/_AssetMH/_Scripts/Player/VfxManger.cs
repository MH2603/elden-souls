using MyBox;
using UnityEngine;

public class VfxManger : MonoBehaviour
{
    [Separator("FxToOn")]
    public ParticleSystem fxGetCoin;
    public ParticleSystem fxUpLevel;

    
    public void Init()
    {

    }
    public void GetReward()
    {
        fxGetCoin.Play();
    }

    public void UpLevel()
    {
        fxUpLevel.Play();   
    }
}
