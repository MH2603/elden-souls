using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData 
{
    //-------- Information ------------
    public string nameUser;


    //---- Stats --------

    public float RatioDmgPlus;

    public float RatioHpPlus;
    public float RatioStaPlus;
    public float RatioManaPlus;

    public int coin;

    public int level;
    public float currentExp;
    //public int ExpToNextLevel;

    public int pointUpStats;

    //----- Status -------------------

    public Vector3 posCurrent;
    //public int idCrWeapon;
    //public int idCrHat;
    //public int idCrArmor;
    //public int idCrPats;

    public int idItemFuture;
    public List<ItemCollected> listItemCollected;


    public PlayerData()
    {
        nameUser = "New Player";

        RatioDmgPlus = 0;
        RatioHpPlus = 0;
        RatioStaPlus = 0;   
        RatioManaPlus = 0;

        coin = 0;

        level = 0;
        currentExp = 0;

        pointUpStats = 0;

        posCurrent = new Vector3(0,0,0);

        //idCrWeapon = -1;
        //idCrHat = -1;
        //idCrArmor = -1; 
        //idCrPats = -1;

        idItemFuture = 0;   
        listItemCollected = new List<ItemCollected>(); 
    }

}

[System.Serializable]
public class ItemCollected
{
    public int idCollected;
    public int idSO;

    public int countCr;

    public enum StatusItem
    {
        None,
        CanUse,
        Lock,
        Equip
    }

    public StatusItem status;

    public ItemCollected() 
    {
        idSO = -1;
        countCr = 1;

        status = StatusItem.None;
    }
}
