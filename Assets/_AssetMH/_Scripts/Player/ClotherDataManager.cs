using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClotherDataManager : MonoBehaviour
{
    public List<PartClotherData> listClotherData;
}

[System.Serializable]
public struct PartClotherData
{
    public PartClother typePart;
    public Mesh mesh;

}
