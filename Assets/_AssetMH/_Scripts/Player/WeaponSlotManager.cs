using System;
using UnityEngine;
using MyBox;
using System.Collections;

public class WeaponSlotManager : MonoBehaviour
{
    public Transform parentWeapon;

    [Separator("Auto Setup")]
    public PlayerStatManager playerStat;
    public ItemCollected weaponCollectedCr;
    [Space]
    public  DmgCollider dmgColliderCr;

    public void LoadWeapon(ItemCollected itemCollected)
    {
        if(dmgColliderCr)
        {
            DataManager.instance.SetStatusItem(weaponCollectedCr.idCollected,
                                        ItemCollected.StatusItem.CanUse);

            Destroy(dmgColliderCr.gameObject);
            dmgColliderCr = null;
        }

        if(itemCollected == null) return;

        weaponCollectedCr = itemCollected;

        WeaponItemSO weaponSO = DataManager.instance.GetDataWeapon(itemCollected.idSO);
        if (weaponSO)
        {
            CreateNewWeapon(weaponSO);
        }

        
    }

    void CreateNewWeapon(WeaponItemSO newWeaponSO)
    {
        GameObject newWeapon = Instantiate(newWeaponSO.prefab, 
                                            parentWeapon.position, parentWeapon.rotation,
                                            parentWeapon);
        newWeapon.SetActive(false); 

        dmgColliderCr = newWeapon.GetComponent<DmgCollider>();

        dmgColliderCr.dmgBase = newWeaponSO.statBase.dmgBase;
    }

    public void OnActiveWeapon() // call by event animator
    {
        dmgColliderCr.gameObject.SetActive(true);   
    }

    void SetDmgForWeapon()
    {
        if( playerStat ) dmgColliderCr.dmgCurrent = dmgColliderCr.dmgBase + playerStat.dmg;
    }


    public void OnWeaponCollider() // call by animation event
    {
        if( !dmgColliderCr ) return;

        SetDmgForWeapon();
        dmgColliderCr.OnCollider();

    }

    public void OffWeaponCollider() // call by animation event
    {
        if (!dmgColliderCr) return;

        dmgColliderCr.OffCollider();
    }
}
