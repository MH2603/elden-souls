
using UnityEngine;

public static class PlayerPrefManager 
{
    private const string Current_Id_Weapon = "Current_Id_Weapon";
    private const string Player_Data = "Player_Data";

    public static int IdCurrentWeapon
    {
        get => PlayerPrefs.GetInt( Current_Id_Weapon, -1);
        set => PlayerPrefs.SetInt(Current_Id_Weapon, value);
    }

    public static void SavePlayerData(string json)
    {
        PlayerPrefs.SetString(Player_Data, json);
    }
    
    public static string LoadSavePlayerData()
    {
        return PlayerPrefs.GetString(Player_Data);
    }
}
