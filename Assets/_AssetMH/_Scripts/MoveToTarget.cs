
using MyBox;
using UnityEngine;
using UnityEngine.Events;

public class MoveToTarget : MonoBehaviour
{
    [Separator("Event")]
    public UnityEvent CompleteEvent;

    [Separator("Stat")]
    public float moveSpeed = 5; //move speed
    public float rotationSpeed = 5; //speed of turning
    public float radiusTarget = 10f;
    public float delayStartTarget = 1f;
    [SerializeField] Vector3 temp = new Vector3(1, 1, 1);
    public Vector3 tempStart = new Vector3(0, 1, 0);
    public float distaneComplete = 0.5f;


    [Separator("Status")]
    public bool readyTarget;
    public GameObject targetOb; //the enemy's target
    
    Vector3 valueMovementOfPlayer;
    float distanceWithTarget;

    public void Init(GameObject newTarget)
    {
        targetOb = newTarget;
        Invoke("StartTarget", delayStartTarget);
    }
    

    void Update()
    {
        CheckMove();
    }

    void StartTarget()
    {
        readyTarget = true;
    }

    void MovementTargetOb()
    {
        //move towards the player
        transform.position += transform.forward * Time.deltaTime * moveSpeed;

        //rotate to look at the player
        transform.rotation = Quaternion.Slerp(transform.rotation, 
            Quaternion.LookRotation(Vector3.Scale(targetOb.transform.position - transform.position, temp))
            , rotationSpeed * Time.deltaTime);
    }

    void MovementForward()
    {
        transform.position += tempStart * Time.deltaTime * moveSpeed; 
    }

    void CheckMove()
    {
        distanceWithTarget = Vector3.Distance(this.transform.position , targetOb.transform.position);

        if( distanceWithTarget <= distaneComplete)
        {
            Complete();
            return;
        }

        if (targetOb != null && readyTarget) MovementTargetOb();
        else MovementForward();
      
    }

    void Complete()
    {
        CompleteEvent?.Invoke();
    }
}
