﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [Separator("Asset")]
    public PrefabsSO prefabsSO;

    [Separator("Component")]
    public PlayerController player;
    public DataManager dataManager;
    public Gui gui;
    public Camera camMain;
    

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }else
        {
            Instance = this;
        }
        DontDestroyOnLoad(this.gameObject);

    }

    private void Start()
    {
        Init();
    }

    void Init()
    {
        dataManager.Init();
        player.Init();
        gui.Init();
    }

    #region( Camera )
    public Transform camTransform()
    {
        return camMain.transform;
    }

    #endregion
}