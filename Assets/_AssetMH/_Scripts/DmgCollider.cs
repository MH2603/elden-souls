using MyBox;
using UnityEngine;
using UnityEngine.Events;

public class DmgCollider : MonoBehaviour
{
    [Separator("Component")]
    [SerializeField] Collider damageCollider;
    public Transform posSpawnBlood;
    public LayerMask layerCanDmg;


    [Separator("Stat")]
    public float dmgBase;
    public float dmgCurrent;

    //public enum TypeWeapon { WeaponOfPlayer, WeaponOfEnemy}
    //public TypeWeapon typeWeapon;   

    [Separator("Vfx")]
    public ParticleSystem trail;
    public ParticleSystem blood;

    public UnityEvent eventCollider;


    private void Awake()
    {
        Init();
    }

    void Init()
    {
        damageCollider = this.GetComponent<Collider>();

        damageCollider.isTrigger = true;
        damageCollider.enabled = false;
    }

    public void OnCollider()
    {
        damageCollider.enabled = true;
        trail.Play();
        Debug.Log("damageCollider.enabled = true");
    }

    public void OffCollider()
    {
        damageCollider.enabled = false;
        trail.Stop();
    }

    

    public void OnTriggerEnter(Collider other)
    {

        if( other.gameObject.layer == layerCanDmg && other.gameObject.GetComponent<Health>())
        {
            eventCollider?.Invoke();

            other.gameObject.GetComponent<Health>().TakeDmg(dmgCurrent);

            Destroy(Instantiate(blood.gameObject, posSpawnBlood.position, posSpawnBlood.rotation), 2f);
        }
    }
}
