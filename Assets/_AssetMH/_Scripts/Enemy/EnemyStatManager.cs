using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatManager : MonoBehaviour
{
    [Separator("Asset")]
    public EnemyStatSO statSO;

    [Separator("Stats")]
    [Header("* Body Stat")]
    public float dmg = 1f;
    public float maxHP = 100f;
    public float speedRun = 2f;

    [Header("* Check ")]
    public float angleCheck = 120f;
    public float distanceCheck = 5f;
    public LayerMask layerOb;
    [Space]
    public float distanceToAttack = 3f;
    public float distanceToPreAttack = 5f;
    public float distanceToMiss = 10f;
    [Space]
    public Vector2 distanceRandomIdle = new Vector2(1, 4);
    public Vector2 timeRandomIdle = new Vector2(1, 3);
    public float speedIdle = 1f;
    [Space]
    public float lookAtSmoothing = 3f;
    public float speedAngleNav = 150f;
    public Vector2 timeRdMovePreAttack = new Vector2(2,5);

    [Separator("Status")]
    public float currentHp;
    public float currentStamina;

    [Separator("RewardWhenDie")]
    public int coinReward = 10;
    public int expReward = 10;

    public void Init()
    {
        LoadStats();

        currentHp = maxHP;
        
    }

    void LoadStats()
    {
        dmg = statSO.dmg;
        maxHP = statSO.maxHP;   
        speedRun = statSO.speedRun; 

        angleCheck = statSO.angleCheck;
        distanceCheck = statSO.distanceCheck;
        layerOb = statSO.layerOb;   

        distanceToAttack = statSO.distanceToAttack; 
        distanceToMiss = statSO.distanceToMiss; 

        distanceRandomIdle = statSO.distanceRandomIdle;
        timeRandomIdle = statSO.timeRandomIdle;
        speedIdle = statSO.speedIdle;

    }
}
