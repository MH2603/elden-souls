using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.AI;

public class EnemyController : CharactersController
{
    [Separator("Component")]
    public WeaponSlotManager weaponManager;
    public EnemyStatManager statManager;
    public Transform posStartCheck;
    public NavMeshAgent nav;
    public Animator anim;

    public List<string> listNameAni;

    [Header("* UI")]
    public EnemyWUI wui;

    [Separator("Status")]
    public GameObject player;
    [SerializeField]private Action m_Status ;

    Vector3 posStart;
    Vector3 pos;

    float speed;
    Vector2 timeRd;
    Vector2 distanceRd;

    public enum Action { Idle, Run, PreAttack , MoveToAttack ,Attack }

    RaycastHit hit;
    float distanceWithPlayer;


    private void Start()
    {
        Init();
    }

    void Init()
    {
        statManager.Init();

        posStart = this.transform.position;
        nav.angularSpeed = statManager.speedAngleNav;
        StatusAction = Action.Idle; 

        wui.Init(); 
    }

    public void Update()
    {
        if (player != null ) CheckMissPlayer(); 

        if (StatusAction == Action.Idle) CheckPlayerWhenIdle(); 
        if (StatusAction == Action.Run ) Running(); 

        if (StatusAction == Action.PreAttack )
        {
            LookAtTarget();
            CheckDistaneAttack();   
        }

        if (StatusAction == Action.MoveToAttack)
        {
            LookAtTarget();
            MoveToAttack();
        }

        anim.SetFloat(listNameAni[0], nav.speed, 0.1f, Time.deltaTime);

    }

    public Action StatusAction
    {
        get { return m_Status; }
        set
        {
            m_Status = value;
            switch(value)
            {
                case Action.Idle:
                    CancelInvoke("MoveToPosRd");
                    Invoke("MoveToPosRd", Random.Range(timeRd.x, timeRd.y));
                    break;

                case Action.Run:
                    StartRun();
                    break;

                case Action.PreAttack:
                    CancelInvoke("MoveToPosRd");
                    Invoke("MoveToPosRd", Random.Range(timeRd.x, timeRd.y));

                    Invoke("TimeToAttack", Random.Range(statManager.timeRdMovePreAttack.x , statManager.timeRdMovePreAttack.y) );
                    break;

                case Action.MoveToAttack:
                    StartMoveToAttack();
                    break;

                case Action.Attack:
                    ChoseTypeAttack();
                    break;
            }
        }
    }
    

    #region(AI Action)

    // idle
    void MoveToPosRd()
    {
        CancelInvoke("MoveToPosRd");
        Invoke("MoveToPosRd", Random.Range(timeRd.x, timeRd.y));

        if (StatusAction == Action.Idle || StatusAction == Action.PreAttack)
        {
            speed = statManager.speedIdle;
            timeRd = statManager.timeRandomIdle;
            distanceRd = statManager.distanceRandomIdle;
            nav.angularSpeed = statManager.speedAngleNav;
            nav.isStopped = false;

            if (StatusAction == Action.PreAttack) nav.angularSpeed = 0;
        }
        else
        {
            CancelInvoke("MoveToPosRd");
            return;
        }


        if (StatusAction == Action.Idle) pos = posStart;
        else if( StatusAction == Action.PreAttack ) pos = this.transform.position;
        
        pos.z += Random.Range(distanceRd.x, distanceRd.y);
        pos.x += Random.Range(distanceRd.x, distanceRd.y);

        nav.speed = statManager.speedIdle;
        nav.SetDestination(pos);

    }

    void CheckPlayerWhenIdle()
    {
        for (int i = 0; i < statManager.angleCheck; i++)
        {

            if (Physics.Raycast(posStartCheck.position, Quaternion.Euler(0, i - statManager.angleCheck / 2, 0) * this.transform.forward, out hit, statManager.distanceCheck, statManager.layerOb))
            {
                player = hit.transform.gameObject;
                StatusAction = Action.Run;
                return;

            }
        }
    }

    void CheckMissPlayer()
    {
        if (distanceWithPlayer >= statManager.distanceToMiss)
        {
            CancelInvoke("MoveToPosRd");
            player = null;
            StatusAction = Action.Idle;
            return;
        }
    }

    // Run
    void StartRun()
    {
        nav.speed = statManager.speedRun;
        return;
    }

    void Running()
    {
        nav.SetDestination(player.transform.position);
        CheckPlayerWhenRun();
    }

    void CheckPlayerWhenRun()
    {
        distanceWithPlayer = Vector3.Distance(player.transform.position, this.transform.position);

        if (distanceWithPlayer <= statManager.distanceToPreAttack)
        {
            StatusAction = Action.PreAttack;
        }
    }


    // PreAttack
    private void LookAtTarget()
    {
        Vector3 dir = player.transform.position - this.transform.position;
        dir.y = 0;
        Quaternion rot = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * statManager.lookAtSmoothing);
    }

    void TimeToAttack()
    {
        CancelInvoke("MoveToPosRd");
        StatusAction = Action.MoveToAttack;
    }

    // MoveToAttack

    void MoveToAttack()
    {
        nav.isStopped = false;
        nav.SetDestination(player.transform.position);
        CheckDistaneAttack();
    }

    void CheckDistaneAttack()
    {
        distanceWithPlayer = Vector3.Distance(transform.position, player.transform.position);
        if (distanceWithPlayer < statManager.distanceToAttack)
        {
            CancelInvoke("TimeToAttack");
            StatusAction = Action.Attack;
        }
    }

    void StartMoveToAttack()
    {
        //nav.angularSpeed = statManager.speedAngleNav;
        nav.speed = statManager.speedRun;
    }



    // Attack
    void ChoseTypeAttack(int type = 0)
    {
        Debug.Log("Chose Type Attack !!!");
        nav.isStopped = true;
        if(type == 0) type = Random.Range(1,3);
        TypeAttack(type);
    }

    void TypeAttack(int index = 0)
    {
        index = Mathf.Clamp(index, 1, 2);   

        switch (index)
        {
            case 1:
                anim.SetInteger(listNameAni[1],1);
                break;
            case 2:
                anim.SetInteger(listNameAni[1], 2);
                break;
        }
            
    }

    public void ExitAttack() // Event Animation
    {
        if (player == null) return;

        distanceWithPlayer = Vector3.Distance(transform.position, player.transform.position);
        anim.SetInteger(listNameAni[1], 0);

        if( distanceWithPlayer <= statManager.distanceToPreAttack)
        {
            StatusAction = Action.PreAttack;
        }
        else
        {
            StatusAction = Action.Idle;
        }
    }


    #endregion

    #region(Take Damage)
    public override void TakeDamage(float dmg = 0)
    {
        if (statManager.currentHp <= 0) return;
        statManager.currentHp -= dmg;

        wui.HpSetFill(statManager.currentHp / statManager.maxHP);
        wui.ShowTextDmg(dmg);

        if(statManager.currentHp <= 0)
        {
            PreDie();
        }

    }
    #endregion

    #region Die

    void RewardForPlayer()
    {
        TrailReward reward = Instantiate( GameManager.Instance.prefabsSO.trailRewardPrefab, 
            transform.position, Quaternion.identity );
        reward.Init(statManager.coinReward, statManager.expReward);
    }

    void PreDie()
    {
        int num = Random.Range(1, 3);
        anim.SetInteger(listNameAni[2], num);

        weaponManager.OffWeaponCollider();
        Destroy(wui.gameObject);
        Destroy(GetComponent<Collider>());
        
    } 

    public void Die() // Event Animation
    {
        RewardForPlayer();
        Destroy(this);
    }

    #endregion


}
