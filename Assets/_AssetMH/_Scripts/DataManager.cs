﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using Unity.VisualScripting;
using JetBrains.Annotations;

public class DataManager : MonoBehaviour
{
    public static DataManager instance { get; private set; }

    [Separator("Component")]
    public PlayerController playerController;

    [Separator("Asset")]
    public DataItemSO dataItem;
    public ValueExpToNextLevelSO expSO;

    [Separator("Status")]
    public PlayerData playerData;


    int idStart;


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);

        Init();
    }


    public void Init()
    {
        playerController = GameManager.Instance.player;

        LoadDataPlayer();
    }

    public void GetCoinAndExp(int coin, int exp)
    {
        playerData.coin += coin;
        playerData.currentExp += exp;

        SavePlayerData();

        CheckNextLevel();
    }

    #region(Level)
    public void CheckNextLevel()
    {
        if (playerData.level > expSO.listValueExp.Count) return;

        if(playerData.currentExp >= expSO.listValueExp[playerData.level])
        {
            NextLevel();

            float ratio = (float)playerData.currentExp / (float)expSO.listValueExp[playerData.level];
            playerController.SetExp(ratio, true);

        }
        else
        {
            float ratio = (float)playerData.currentExp / (float)expSO.listValueExp[playerData.level];
            playerController.SetExp(ratio, false);
        }

        
    }

    void NextLevel()
    {
        playerData.currentExp -= expSO.listValueExp[playerData.level];
        playerData.level++;

        playerData.pointUpStats += playerController.statManager.statSO.countPointPlus;
        
        
        SavePlayerData();
    }
    #endregion

    #region (Load/Save) Data
    public void LoadDataPlayer()
    {
        string json = PlayerPrefManager.LoadSavePlayerData();
        if (string.IsNullOrEmpty(json))
        {
            playerData = new PlayerData();
            Debug.Log("--> New Player Data !!!");
        }
        else
        {
            Debug.Log("--> Load Old Player Data !!!");
            playerData = JsonUtility.FromJson<PlayerData>(json);
        }
    }

    public void SavePlayerData()
    {
        Debug.Log("--> Save Player Data !!!");
        string json = JsonUtility.ToJson(playerData);
        PlayerPrefManager.SavePlayerData(json);
    }
    #endregion

    #region( Item Collection and Equipment )
    public ItemCollected AddItemCollectionToData(int id, int count = 1)
    {
        ItemCollected newItemCollection = new ItemCollected();

        newItemCollection.idCollected = playerData.idItemFuture;
        playerData.idItemFuture++;

        newItemCollection.status = ItemCollected.StatusItem.CanUse;
        newItemCollection.idSO = id;

        playerData.listItemCollected.Add(newItemCollection);

        SavePlayerData();

        return newItemCollection;
    }

    public void CheckToEquipItem( int idCollected, bool ToEquip = true)
    {
        if(ToEquip) SetStatusItem(idCollected, ItemCollected.StatusItem.Equip);
        else SetStatusItem(idCollected, ItemCollected.StatusItem.CanUse);

        SavePlayerData();
    }

    int CheckListItemOfPlayer(int id)
    {
        return 0;
    }

    #endregion

    #region(Get Data Item)
    public ItemSO GetDataItemSO(int id) 
    {
        for(int i = 0; i < dataItem.listItems.Length; i++) 
        {
            if( id == dataItem.listItems[i].id ) 
                return dataItem.listItems[i];
        }

        Debug.LogError("Tao ko tìm thấy item nào có id la : " + id);
        return null;    
    }

    public ItemSO GetItemSO_ByIdCollected(int idCollected)
    {
        if(GetItemCollected(idCollected) != null)
        {
            return GetDataItemSO(GetItemCollected(idCollected).idSO);
        }

        return null;
    }

    public EquipmentItemSO GetEquipmentItemSO(int idEqm)
    {
        for (int i = 0; i < dataItem.listEquipment.Length; i++)
        {
            if (idEqm == dataItem.listEquipment[i].id)
            {
                return dataItem.listEquipment[i];
            }
        }

        Debug.LogError("Tao ko tim thay Equipment voi id la : " + idEqm);
        return null;
    }

    public WeaponItemSO GetDataWeapon(int idWeapon)
    {
        for (int i = 0; i < dataItem.listWeapon.Length; i++)
        {
            if (idWeapon == dataItem.listWeapon[i].id)
            {
                return dataItem.listWeapon[i];
            }
        }

        Debug.LogError("Tao ko tim thay Weapon voi id la : " + idWeapon);
        return null;
    }

    public ClotherItemSO GetDataClother(int idClother)
    {
        for (int i = 0; i < dataItem.listClother.Length; i++)
        {
            if (idClother == dataItem.listClother[i].id) return dataItem.listClother[i];
        }

        Debug.LogError("Tao ko tim thay Clother voi id la: " + idClother);
        return null;
    }

    public ItemCollected GetItemCollected(int id)
    {
        foreach( ItemCollected item in playerData.listItemCollected)
        {
            if(id == item.idCollected)
            {
                return item;
            }
        }

        Debug.LogError("Tao ko tim thay Item Collected voi id la: " + id);
        return null;
    }

    public void SetStatusItem(int id, ItemCollected.StatusItem status)
    {
        if(GetItemCollected(id) != null)
        {
            GetItemCollected(id).status = status;
            SavePlayerData();

            //Debug.Log("--> Step 02 " + " : " + id + " -- " + status);

            return;
        }
    }

    #endregion

    #region(Btn Method)
    [ButtonMethod]
    public void Save_Custom()
    {
        SavePlayerData();
    }

    [ButtonMethod]
    public void Load_Custom()
    {
        LoadDataPlayer();   
    }

    #endregion
}





