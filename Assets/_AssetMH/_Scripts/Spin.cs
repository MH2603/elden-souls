using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    public Vector3 speed = new Vector3( 0,0,5);

    public void FixedUpdate()
    {
        this.transform.Rotate(speed * Time.deltaTime);  
    }
}
