using System;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public float hpMax;
    public float hpCurrent;
    public bool NoGetDmg;

    public UnityEvent eventDie;
    public UnityEvent eventTakeDmg;  

    public void Init(UnityAction call,float maxHp, float currentHp )
    {
        eventDie.AddListener(call);
        hpMax = maxHp;
        hpCurrent = currentHp;  
    }

    public void OnNoGetDmg(float timeOff)
    {
        if (timeOff>0)
        {
            NoGetDmg = true;

            CancelInvoke("OffNoGetDmg");
            Invoke("OffNoGetDmg",timeOff);
        }
    }

    public void OffNoGetDmg()
    {
        NoGetDmg = false;
    }


    public void TakeDmg(float dmg)
    {
        if( hpCurrent <= 0 || NoGetDmg) return ;  

        hpCurrent -= dmg;
        eventTakeDmg?.Invoke();

        if (hpCurrent <= 0)
        {
            hpCurrent = 0;
            eventDie?.Invoke(); 
        }
    }

}


